import { ApiRequest } from "../request";

let auth = {
    email: "pirat1400@ukr.net",
    password: "KnewLess1"
};

let accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZDUxNmQ5Yy0yNjUzLTQ0NDQtODY1YS0xN2ExMGM2Y2I5YTAiLCJpYXQiOjE2NTkwMzcxNTEsImV4cCI6MTY1OTEyMzU1MX0.AvtkuQNVdYgEHnaDWMZ_GPWxM6Cj5ExHBhVTr60SSMt0vU5TieQXZMnQsiVsHPXjGt4uvMqvyt-GbkJQzN3oWg";
let baseUrl = "https://knewless.tk/api/";

export class UserController {
    async authorization() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`auth/login`)
            .body(auth)
            .send();
        return response;
    }

    async show_profile() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .url(`student`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async show_recomemendet_courses() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/recommended`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async show_all_courses() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/all`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async show_student_profile() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .url(`student/profile`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
