import { ApiRequest } from "../request";

let auth = {
    email: "pirat1400@ukr.net",
    password: "KnewLess1"
};

let invalid_auth = {
    email: "pirat1400@ukr.net",
    password: ""
};

let accesToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZDUxNmQ5Yy0yNjUzLTQ0NDQtODY1YS0xN2ExMGM2Y2I5YTAiLCJpYXQiOjE2NTg3NzE2ODYsImV4cCI6MTY1ODg1ODA4Nn0.33HgaChSm5o2zCzXVcJ50W4R7kD4ACQafbw4yQfFxmZHgsdjG-ut4hC2lpYA2ytSoRiokiByKf_QznGRQqPv3g";

let baseUrl = "https://knewless.tk/api/";

export class StudentController {
    async authorization() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`auth/login`)
            .body(auth)
            .send();
        return response;
    }

    async authorization_with_invalid_data() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`auth/login`)
            .body(invalid_auth)
            .send();
        return response;
    }

    async show_recomemendet_courses() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/recommended`)
            .bearerToken(accesToken)
            .send();
        return response;
    }

    async show_all_courses() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/all`)
            .bearerToken(accesToken)
            .send();
        return response;
    }

    async show_student_profile() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .url(`student/profile`)
            .bearerToken(accesToken)
            .send();
        return response;
    }
}
