import { ApiRequest } from "../request";

export class CoursesController {
    async getAllCourses() {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("GET")
            .url(`course/all`)
            .send();
        return response;
    }

    async getPopularCourses() {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("GET")
            .url(`course/popular`)
            .send();
        return response;
    }

    async getAllCourseInfoById(id: string) {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("GET")
            .url(`course/${id}/info`)
            .send();
        return response;
    }
}
