import { ApiRequest } from "../request";

let auth = {
    email: "knewless@ukr.net",
    password: "KnewLess1"
};
let token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjZWI5N2YzZi00MDZhLTQwNmYtODMzMC0zZGYyZWE5ZmQ3MjciLCJpYXQiOjE2NTg3NzI5MjAsImV4cCI6MTY1ODg1OTMyMH0.yHrAEx0AFEnYlpy4nGGA56RgqApBw43OewvcTwAn--ka5QewFyDCkfJXTtJVvGBq65jCwVsIlG5Z1F6Qf4NwwQ";

let baseUrl = global.appConfig.baseUrl;

export class AuthorController {
    async authorization() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`auth/login`)
            //.email(auth.email)
            //.password(auth.password)
            .body(auth)
            //.headers(auth)
            //.headers(token)
            //.bearerToken(token)
            .send();
        return response;
    }

    async author_get() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`author`)
            .body(auth)
            .send();
        return response;
    }

    async author_post() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`author`)
            .body(auth)
            .send();
        return response;
    }

    async author_me() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`author`)
            .bearerToken(token)
            .send();
        return response;
    }

    async author_overview() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`author/overview`)
            .body(auth)
            .send();
        return response;
    }

    async author_article() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`article/`)
            .body(auth)
            .send();
        return response;
    }

    async author_article_author() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`article/author`)
            .body(auth)
            .send();
        return response;
    }

    async author_article_id() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`article/`)
            .body(auth)
            .send();
        return response;
    }

    async author_article_comment() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`article_comment/`)
            .body(auth)
            .send();
        return response;
    }

    async author_article_comment_size() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`article_comment/of`)
            .body(auth)
            .send();
        return response;
    }
}