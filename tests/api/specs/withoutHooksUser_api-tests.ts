import { expect } from "chai";
import { checkStatusCode, checkResponseBodyStatus, checkResponseBodyMessege, checkResponseTime } from '../../helpers/helpers';
import { UserController } from "../lib/controlers/user.controler";
const chai = require("chai");
chai.use(require("chai-json-schema"));
const student = new UserController();
const schemas = require("./data/schemas_student_login.json");


describe("User Controller", () => {
    let accessToken: string, userId: string;



    it("Successful Login", async () => {
        let response = await student.authorization();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.student_authorization);
        userId = response.body.accessToken;
    });
    it("Authorization with invalid data", async () => {
        let response = await student.authorization();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.student_authorization);
    });
    /*
    it("Show recomemendet courses", async () => {
        let response = await student.show_recomemendet_courses();
        //console.log(response);
        expect(response.statusCode, "Status code should be 200").to.be.equal(200);
        expect(response.timings.phases.total, "Response time should be less than 1s").to.be.lessThan(1000);
        expect(response.body).to.be.jsonSchema(schemas.recommented_courses);
    });
    it("Show all courses", async () => {
        let response = await student.show_all_courses();
        //console.log(response);
        expect(response.statusCode, "Status code should be 200").to.be.equal(200);
        expect(response.timings.phases.total, "Response time should be less than 1s").to.be.lessThan(1000);
        expect(response.body).to.be.jsonSchema(schemas.show_all_courses);
    });
    it("Show all courses", async () => {
        let response = await student.show_student_profile();
        //console.log(response);
        expect(response.statusCode, "Status code should be 200").to.be.equal(200);
        expect(response.timings.phases.total, "Response time should be less than 1s").to.be.lessThan(1000);
        expect(response.body).to.be.jsonSchema(schemas.student_profile);
    });
    */
});



