import { expect } from "chai";
import { AuthorController } from "../lib/controlers/author.controler";
import { checkStatusCode, checkResponseBodyStatus, checkResponseBodyMessege, checkResponseTime } from '../../helpers/helpers';
const chai = require("chai");
chai.use(require("chai-json-schema"));
const author = new AuthorController();
const schemas = require("./data/schemas_author_login.json");


describe("Author Controller", () => {
    it("Authorization", async () => {
        let response = await author.authorization();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.author_login);
    });
    /*
    it("Author GET", async () => {
        let response = await author.author_get();
        //console.log(response);
        expect(response.statusCode, "Status code should be 200").to.be.equal(200);
        expect(response.timings.phases.total, "Response time should be less than 1s").to.be.lessThan(1000);
        expect(response.body).to.be.jsonSchema(schemas.author_login);
    });
    
    it("Author POST", async () => {
        let response = await author.author_post();
        //console.log(response);
        expect(response.statusCode, "Status code should be 200").to.be.equal(200);
        expect(response.timings.phases.total, "Response time should be less than 1s").to.be.lessThan(1000);
        expect(response.body).to.be.jsonSchema(schemas.author_login);
    });
    
    it("Author me", async () => {
        let response = await author.author_me();
        console.log(response);
        expect(response.statusCode, "Status code should be 401").to.be.equal(401);
        expect(response.timings.phases.total, "Response time should be less than 1s").to.be.lessThan(1000);
        expect(response.body).to.be.jsonSchema(schemas.author_Unauthorized);
    });
    it("Author overview me", async () => {
        let response = await author.author_overview();

    });
    it("Author article", async () => {
        let response = await author.author_article();

    });
    it("Author article author", async () => {
        let response = await author.author_article_author();

    });
    it("Author article id", async () => {
        let response = await author.author_article_id();

    });
    it("Author article comment", async () => {
        let response = await author.author_article_comment();

    });
    it("Author article comment size", async () => {
        let response = await author.author_article_comment_size();

    });

*/
});
