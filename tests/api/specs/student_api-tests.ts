import { expect } from "chai";
import { checkStatusCode, checkResponseBodyStatus, checkResponseBodyMessege, checkResponseTime } from '../../helpers/helpers';
import { StudentController } from "../lib/controlers/student.controler";
const chai = require("chai");
chai.use(require("chai-json-schema"));
const student = new StudentController();
const schemas = require("./data/schemas_student_login.json");


describe("Student Controller", () => {
    it("Successful Login", async () => {
        let response = await student.authorization();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.student_authorization);
    });
    it("Authorization with invalid data", async () => {
        let response = await student.authorization_with_invalid_data();

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.student_authorization_with_invalid_data);
    });
    /*
    it("Show recomemendet courses", async () => {
        let response = await student.show_recomemendet_courses();
        //console.log(response);
        expect(response.statusCode, "Status code should be 200").to.be.equal(200);
        expect(response.timings.phases.total, "Response time should be less than 1s").to.be.lessThan(1000);
        expect(response.body).to.be.jsonSchema(schemas.recommented_courses);
    });
    it("Show all courses", async () => {
        let response = await student.show_all_courses();
        //console.log(response);
        expect(response.statusCode, "Status code should be 200").to.be.equal(200);
        expect(response.timings.phases.total, "Response time should be less than 1s").to.be.lessThan(1000);
        expect(response.body).to.be.jsonSchema(schemas.show_all_courses);
    });
    it("Show all courses", async () => {
        let response = await student.show_student_profile();
        //console.log(response);
        expect(response.statusCode, "Status code should be 200").to.be.equal(200);
        expect(response.timings.phases.total, "Response time should be less than 1s").to.be.lessThan(1000);
        expect(response.body).to.be.jsonSchema(schemas.student_profile);
    });
    */
});



