import { expect } from 'chai';

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseBodyStatus(response, status: string) {
    expect(response.body.status, `Status should be ${status}`).to.equal(status);
}

export function checkResponseBodyMessege(response, messege: string) {
    expect(response.body.messege, `Messege should be ${messege}`).to.equal(messege);
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(maxResponseTime);
}