# Owner
[Academy binary studio](https://academy.binary-studio.com/)


## Знову використовуємо проект минулорічної академії Knewless. http://knewless.tk/
Ми вже знайомі із його свагером і робили запити до цього API. Тепер спробуємо написати автоматичні тести у нашому фреймворку. Завдання полягає у наступному:

Створити тестовий проект на вашому комп’ютері (повторити кроки із лекції по створенні фреймворку з нуля)

Написати авто тести для наступного ланцюжка запитів (використовуйте юзера з роллю автора):
- POST {url}/api/login
- POST {ur}//api/author/
- GET {url}/api/user/me
- GET {url}/api/author/overview/{userId}
- POST {url}/api/article/
- GET{url}/api/article/author
- GET {url}/api/article/{atricleId}
- POST{url}/api/article_comment
- GET{url}/api/article_comment/of/{atricleId}?size=200

Придумати подібний ланцюжок для юзера з роллю: “Student” з 6-7 запитами (не більше)
Додати негативні тести (не більше 3-х)
Тести повинні містити перевірку статус коду, часу відповіді, валідацію схеми, даних(де можливо).

# Зареєструватись у GitLab
Створити там public репозиторій та залити код
Надіслати посилання на ваш репозиторій (edited)