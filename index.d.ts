export { };
declare global {
    namespace nodeJS {
        interface Global {
            appconfig: {
                envName: string,
                baseUrl: string,
                swaggerUrl: string,
                users: object,
            };
        }
    }
}
